/*****************************************************************
                        Project description
******************************************************************
Code for Rotating Rocker platform

Written by:Bjorn de Wagenaar
Mimetas - Organ on a Chip company
Version 0.1 181011
*****************************************************************/


/*****************************************************************
                        Include libraries
*****************************************************************/
#include "Adafruit_MCP23017.h"      // LCD library
#include "Adafruit_RGBLCDShield.h"  // LCD Library
#include "math.h"                   // Math Library

/*****************************************************************
                  A/D pin & adress definitions
*****************************************************************/
#define PIN_SDA         0       // SDA pin I2C
#define PIN_SCL         1       // SCL pin I2C
#define PIN_M_DIR       6       // Motor pin direction
#define PIN_M_STEP      7       // Motor pin step

/*****************************************************************
                      Initiate libraries
*****************************************************************/
SYSTEM_MODE(AUTOMATIC);               // System mode in semi automatic, so it can run offline when connection is lost
Adafruit_RGBLCDShield lcd = Adafruit_RGBLCDShield();
/*****************************************************************
                        Global vars
*****************************************************************/

int buttonPush = 200;
double microStep = 0.25;
double anglePerStep = microStep * 1.8 / 5.18;
double stepsPerRevolution = 360 / anglePerStep;
double RPM = 1;
double stepDelay = 60 / stepsPerRevolution / RPM * 1000 *1000;

unsigned long startMillis;  //some global variables available anywhere in the program
unsigned long currentMillis;
const unsigned long period = 10000;  //the value is a number of milliseconds
volatile bool direction = LOW;
/*****************************************************************
                          Void setup
*****************************************************************/

void setup(){
    // Setting up Serial com, lcd, wire com, ADXL345 com & settings
    Serial.begin(9600);
    Wire.begin();
    lcd.begin(16, 2);
    lcd.setCursor(0,0);
    lcd.print("RotatingRocker");
    lcd.setCursor(0,1);
    lcd.print("RPM = "); lcd.print(RPM);

    // Defining pinMode motor pins, well volume sensors
    pinMode(PIN_M_DIR, OUTPUT);     // Controls direction
    pinMode(PIN_M_STEP, OUTPUT);     // Controls power to motor

    digitalWrite(PIN_M_DIR,LOW);
    digitalWrite(PIN_M_STEP,LOW);

}

/*****************************************************************
                          Void loop
*****************************************************************/
void loop() {
    startMillis = millis();
    ReadButtons();
      digitalWrite(PIN_M_STEP,HIGH);
      delayMicroseconds(((stepDelay)-(millis()-startMillis)*1000));
      digitalWrite(PIN_M_STEP,LOW);
    }


/*****************************************************************
                        Read LCD buttons
*****************************************************************/
void ReadButtons(){  //read buttons status
   uint8_t buttons = lcd.readButtons();

   if (buttons) {
       if (buttons & BUTTON_UP){
           RPM = constrain(RPM,0.1,2);
           RPM = RPM + 0.05;
           stepDelay = 60 / stepsPerRevolution / RPM *1000*1000;
           delay(buttonPush);
       }
       if (buttons & BUTTON_DOWN){
           RPM = constrain(RPM,0.1,2);
           RPM = RPM - 0.05;
           stepDelay = 60 / stepsPerRevolution / RPM *1000*1000;
           delay(buttonPush);
       }
       if (buttons & BUTTON_LEFT){
           RPM = constrain(RPM,0.1,2);
           RPM = RPM - 0.25;
           stepDelay = 60 / stepsPerRevolution / RPM *1000*1000;
           delay(buttonPush);
       }
       if (buttons & BUTTON_RIGHT){
           RPM = constrain(RPM,0.1,2);
           RPM = RPM + 0.25;
           stepDelay = 60 / stepsPerRevolution / RPM *1000*1000;
           delay(buttonPush);
       }
       if (buttons & BUTTON_SELECT){
           delay(buttonPush);
       }
       lcd.setCursor(0,1);
       lcd.print("RPM = "); lcd.print(RPM);
   }
}
