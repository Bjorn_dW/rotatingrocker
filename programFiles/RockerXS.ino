/*
 * Project RockerXS
 * Description:
 * Author: Bjorn de Wagenaar, Mimetas
 * Date: 180617
 */

 SYSTEM_MODE(AUTOMATIC);  // Photon particle automaticcally connects to the cloud
 //SYSTEM_MODE(SEMI_AUTOMATIC); // Offline mode

 /*****************************************************************
                         Include libraries
 *****************************************************************/
 #include "math.h"
 #include "ADXL345.h"
 #include "Adafruit_GFX_RK.h"
 #include "Adafruit_LEDBackpack_RK.h"

 /*****************************************************************
                   A/D pin & adress definitions
 *****************************************************************/
 #define PIN_SDA         0       // SDA pin I2C
 #define PIN_SCL         1       // SCL pin I2C
 #define PIN_SW1         2       // End swithc 1
 #define PIN_SW2         3       // End switch 2
 #define PIN_M_DIR       6       // Motor pin direction
 #define PIN_M_STEP      7       // Motor pin step
 #define PIN_EN1_1       A0      // Encoder 1 pin 1
 #define PIN_EN1_2       A1      // Encoder 1 pin 2
 #define PIN_EN1_SW      A2      // Encoder 1 pin switch
 #define PIN_EN2_1       A5      // Encoder 2 pin 1
 #define PIN_EN2_2       A4      // Encoder 2 pin 2
 #define PIN_EN2_SW      A3      // Encoder 2 pin switch


 #define ADXLaddr        0x53    // Adress of ADXL345 sensor
 #define QUADaddr        0x70    // Adress of QUAD display

 #define memProgAddr     0x01    // First byte of EEPROM memory (progStatus)
 #define memAngleAddr    0x02    // angle memory adress
 #define memIntervalAddr 0x03    // Interval memory adress



void encoder1UP();
void encoder1DOWN();
void encoder2UP();
void encoder2DOWN();

volatile bool A_set = false;
volatile bool B_set = false;

volatile int encoderPos = 0;
volatile bool direction = LOW;

volatile bool iAngle = false;
volatile bool iInterval = false;

int angle = 0;
int angleSteps;
unsigned long interval = 0;
int counter = 0;
int maxCount = 10;
int iteration = 0;
char displaybuffer[4] = {' ', ' ', ' ', ' '};
bool firstRun = true;
unsigned long previousMillis = 0;
float microStepping = 1/4;

unsigned long startMillis;  //some global variables available anywhere in the program
unsigned long currentMillis;
const unsigned long period = 10000;  //the value is a number of milliseconds

ADXL345 Accel;
Adafruit_AlphaNum4 alpha4 = Adafruit_AlphaNum4();

/*****************************************************************
                           Void setup
*****************************************************************/
void setup() {
  Serial.begin(9600);
  alpha4.begin(QUADaddr);  // pass in the address
  alpha4.clear();
  alpha4.setBrightness(5);
  Accel.set_bw(ADXL345_BW_12);

  pinMode(PIN_M_DIR, OUTPUT); // Controls DIRECTION
  pinMode(PIN_M_STEP, OUTPUT); // Controls STEP
  digitalWrite(PIN_M_DIR, LOW);
  digitalWrite(PIN_M_STEP, LOW);

  pinMode(PIN_EN1_1, INPUT_PULLUP);
  pinMode(PIN_EN1_2, INPUT_PULLUP);
  pinMode(PIN_EN1_SW, INPUT_PULLUP);
  pinMode(PIN_EN2_SW, INPUT_PULLUP);

  attachInterrupt(PIN_EN1_1, encoder1UP, CHANGE);
  attachInterrupt(PIN_EN1_2, encoder1DOWN, CHANGE);
  //attachInterrupt(PIN_SW1, endSwitch1, FALLING);
  //attachInterrupt(PIN_SW2, endSwitch2, FALLING);

  scrollDisplay("MIMETAS ROCKER XS    ");
  calibration();

  setAngle();
  setInterval();

  angleSteps = round(2*angle/0.35/0.25);    // Depending on motor control setting, 0.35 degrees in full stepping mode

  attachInterrupt(A2, changeAngle, CHANGE);
  attachInterrupt(A3, changeInterval, CHANGE);
  startMillis = millis();  //initial start time
}

/*****************************************************************
                            Void loop
*****************************************************************/
void loop() {
    currentMillis = millis();  //get the current "time" (actually the number of milliseconds since the program started)
    if (iAngle == true){
      setAngle();
      iAngle = !iAngle;
    }

    if (iInterval == true){
      setInterval();
      iInterval= !iInterval;
    }

    if(digitalRead(PIN_SW1) == 0 || digitalRead(PIN_SW2) == 0){
      direction = !direction;
    }

    runProgram();
}


/*****************************************************************
                          Void runProgram
*****************************************************************/
void runProgram(){
    printDisplay("P1");
    if (firstRun == true){
      runMotorCount(2000,direction,angleSteps/2);
      direction = !direction;
      firstRun = !firstRun;
    }

    if (currentMillis - startMillis >= interval*10000){
      runMotorCount(2000,direction,angleSteps);
      direction = !direction;
      startMillis = currentMillis;
    }
}

/*****************************************************************
                Void encoder1UP after rotation
*****************************************************************/
void encoder1UP(){
  if( digitalRead(PIN_EN1_1) != A_set ) {  // debounce once more
    A_set = !A_set;
    // adjust counter + if A leads B
    if ( A_set && !B_set )
      encoderPos += 1;
  }
}

/*****************************************************************
              Void encoder1DOWN after rotation
*****************************************************************/
void encoder1DOWN(){
   if( digitalRead(PIN_EN1_2) != B_set ) {
    B_set = !B_set;
    //  adjust counter - 1 if B leads A
    if( B_set && !A_set )
      encoderPos -= 1;
  }
}

/*****************************************************************
                        Void endSwitch1
*****************************************************************/
void endSwitch1(){
  direction = !direction;
}

/*****************************************************************
                         Void endSwitch2
*****************************************************************/
void endSwitch2(){
  direction = !direction;
}

/*****************************************************************
                         Void runMotor
*****************************************************************/
void runMotor(int delay, bool direction){
    digitalWrite(PIN_M_DIR,direction);
    digitalWrite(PIN_M_STEP,HIGH);
    delayMicroseconds(delay);
    digitalWrite(PIN_M_STEP,LOW);
    delayMicroseconds(delay);
}

/*****************************************************************
                      Void runMotorCount
*****************************************************************/
void runMotorCount(int delay, bool direction, int setCount){
    int counter = 0;
    while (counter < setCount){
        digitalWrite(PIN_M_DIR,direction);
        digitalWrite(PIN_M_STEP,HIGH);
        delayMicroseconds(delay);
        digitalWrite(PIN_M_STEP,LOW);
        delayMicroseconds(delay);
        counter++;
    }
}

/*****************************************************************
             Void calibrationXS between end switches
*****************************************************************/
void calibration(){
    scrollDisplay("CALIBRATING....");
    direction = LOW;
    while (digitalRead(PIN_SW1) == 1 && digitalRead(PIN_SW2) == 1){
      runMotor(2000, direction);
    }

    while (digitalRead(PIN_SW1) == 0){
      direction = LOW;
      runMotor(2000,direction);
    }

    while (digitalRead(PIN_SW2) == 0){
      direction = HIGH;
      runMotor(2000,direction);
    }

    while (digitalRead(PIN_SW1) == 1 && digitalRead(PIN_SW2) == 1){
      counter = counter + 1;
      runMotor(2000, direction);
    }

    direction = !direction;
    runMotorCount(2000, direction, counter/2);

    scrollDisplay("CALIBRATION DONE");
}

/*****************************************************************
                Void changeAngle after interrupt
*****************************************************************/
void changeAngle(){
  iAngle = true;
}

/*****************************************************************
              Void changeInterval after interrupt
*****************************************************************/
void changeInterval(){
  iInterval = true;
}

/*****************************************************************
                      Void setAngle
*****************************************************************/
void setAngle(){
  encoderPos = 7;
  scrollDisplay("SET ANGLE    ");
  char *one = "A=";
  char valueString[20]; //
  while(digitalRead(PIN_EN1_SW)==1){
    angle = constrain(encoderPos,0,14);
    sprintf(valueString, "%s%d", one, angle);
    printDisplay(valueString);
    delay(100);
  }
  char *R = "->A=";
  char *S = "o";
  char strAngle[20];
  sprintf(strAngle,"%s%d%s", R, angle, S);
  scrollDisplay(strAngle);
}
/*****************************************************************
                      Void setInterval
*****************************************************************/
void setInterval(){
  encoderPos = 8;
  scrollDisplay("SET INTERVAL    ");
  char *one = "I=";
  char valueString[20]; //
  while(digitalRead(PIN_EN2_SW)==1){
    interval = constrain(encoderPos,0,60);
    sprintf(valueString, "%s%d", one, interval);
    printDisplay(valueString);
    delay(100);
  }
  char *R = "->I=";
  char *S = " MIN";
  char strInterval[20];
  sprintf(strInterval,"%s%d%s", R, interval, S);
  scrollDisplay(strInterval);
}
/*****************************************************************
                      Void printDisplay
*****************************************************************/
void printDisplay(char* str){
  alpha4.clear();
  int pos = 0;
  for (int index = 0; index < strlen(str); index++){      //for each character in str{
    alpha4.writeDigitAscii(pos, str[index]);  //write the character
    pos++;
  }
  alpha4.writeDisplay();  //write to the display.
}

/*****************************************************************
                      Void scrollDisplay
*****************************************************************/
void scrollDisplay(char *str){
  alpha4.clear();
  // display every character
  for (int index = 0; index < strlen(str)-3; index++){      //for each character in str{
    alpha4.writeDigitAscii(0, str[index]);
    alpha4.writeDigitAscii(1, str[index+1]);
    alpha4.writeDigitAscii(2, str[index+2]);
    alpha4.writeDigitAscii(3, str[index+3]);
    alpha4.writeDisplay();  //write to the display.
    delay(250);
  }
  delay(1000);
  alpha4.clear();
}

/*****************************************************************
                      Void breathDisplay
*****************************************************************/
void breathDisplay(char *str, int cycles){
  alpha4.clear();
  printDisplay(str);
  for (int j = 1; j<cycles; j++){
    for (int i = 1; i < 16; i++){
      alpha4.setBrightness(i);
      delay(25);
    }
    for (int i = 15; i > 0; i--){
      alpha4.setBrightness(i);
      delay(25);
    }
  }
}
