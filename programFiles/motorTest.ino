int Distance = 0; // Record the number of steps taken by the motor

void setup(){
  pinMode(6, OUTPUT); // Controls direction
  pinMode(7, OUTPUT); // Controls power to motor
  digitalWrite(6,LOW);
  digitalWrite(7,LOW);
}

void loop(){
  digitalWrite(6,HIGH);
  digitalWrite(7,HIGH);
  delayMicroseconds(100);
  digitalWrite(7,LOW);
  delayMicroseconds(100);
}
