/*****************************************************************
                        Project description
******************************************************************

This software controls the motor control, LCD display & buttons,
ADXL345 accelerometer and the menu structure of the Mimetas
p(rogrammable)Rocker v1.0. The program starts with the menu
library in which the user is able to define the rocking parameters
and the required flow program. After adjusting the parameters, the
right program can be selected by pressing the OK button. The software
will switch to the program loop and will run the program until the
Rocker is reset.

Menu structure:

Main Menu
    Program 1 (select) --> Mimetas P1 program (step waveform pattern)
      Angle
        Angle (set & select)
      Interval
        Interval (set & select)
      Switch duration
        Switch select (set & select)

    Program 2 (select) --> Continous rocking (sin waveform pattern)
      Angle
        Angle (set & select)

    Program 3 (select) --> Unidirectional shear (sawtooth waveform pattern)
      Angle
        Angle (Set & select)
      Interval
        Interval (set & select)

    Program 4 (select) --> Continous flow rate model (triangular waveform pattern)
      Angle
        Angle (set & select)

    Info
      Version 1.0
    Credits
      Mimetas

Written by:
Bjorn de Wagenaar
Mimetas - Organ on a Chip company
Version 1.3 20171212 BW
*****************************************************************/

/*****************************************************************
                        Include libraries
*****************************************************************/
#include "Adafruit_MCP23017.h"      // LCD library
#include "Adafruit_RGBLCDShield.h"  // LCD Library
#include "MenuBackend.h"            // Menu library
#include "math.h"                   // Math Library
#include "ADXL345.h"                // Accelerometer library

/*****************************************************************
                  A/D pin & adress definitions
*****************************************************************/
#define PIN_F_W1        A0      // Voltage divider sensor well 1
#define PIN_F_W2        A1      // Voltage divider sensor well 2
#define PIN_HOME        A2      // Home sensor
#define PIN_SDA         0       // SDA pin I2C
#define PIN_SCL         1       // SCL pin I2C
#define PIN_M_DIR       5       // Motor pin direction
#define PIN_M_SPEED     6       // Motor pin speed
#define ADXLaddr        0x53    // Adress of ADXL345 sensor
#define memProgAddr     0x01    // First byte of EEPROM memory (progStatus)
#define memAngleAddr    0x02    // angle memory adress
#define memIntervalAddr 0x03    // Interval memory adress
#define memSwitchAddr   0x04    // Switch duration memory adress
#define memSpeedAddr    0x05    // Speed duration memory adress
/*****************************************************************
                      Initiate libraries
*****************************************************************/
STARTUP(WiFi.selectAntenna(ANT_EXTERNAL)); // Use external antenna for wifi connection
SYSTEM_MODE(SEMI_AUTOMATIC);               // System mode in semi automatic, so it can run offline when connection is lost
//SYSTEM_MODE(AUTOMATIC);
ADXL345 Accel;
Adafruit_RGBLCDShield lcd = Adafruit_RGBLCDShield();
/*****************************************************************
                        Global vars
*****************************************************************/
int k,X,Y,Z, varselect, counter, counter2, lastButtonPushed = 0, progStatus = 0, prog3_timing = 1, timer = 0, timer_standby = 0, time_refresh = 0;
int angle, interval, switch_duration, speed, buttonPush = 200, meas1, meas2;
double ofX=0,ofY=0,ofZ=0, mofX=-14, mofY=0, mofZ=-21, pitch, angle_margin = 0.25, homePos = 1;
double step, conversion, stepTotal, stepCalc;
uint8_t buttons;
bool direction = LOW, progShowProgram = 0, setAngleStatus = 0, programReset = 0;
String Str;

byte car1 [8] = {0b01111,0b11111,0b11111,0b01111,0b00000,0b00000,0b01111,0b11111};
byte car2 [8] = {0b11111,0b11111,0b11111,0b11111,0b00000,0b00000,0b11110,0b11111};
byte car3 [8] = {0b11110,0b11111,0b11111,0b11110,0b00000,0b00000,0b00000,0b00000};
byte car4 [8] = {0b11111,0b01111,0b00000,0b00000,0b01111,0b11111,0b11111,0b01111};
byte car5 [8] = {0b11111,0b11110,0b00000,0b00000,0b11111,0b11111,0b11111,0b11111};
byte car6 [8] = {0b00000,0b00000,0b00000,0b00000,0b11110,0b11111,0b11111,0b11110};

/*****************************************************************
                 MenuBackend (menustructure)
*****************************************************************/
MenuBackend menu = MenuBackend(menuUseEvent,menuChangeEvent);
//beneath is list of menu items needed to build the menu

MenuItem Item1= MenuItem("Program 1");
MenuItem Item1Sub1 = MenuItem("Angle");
MenuItem Item1Sub1Sub1 = MenuItem("Angle =");
MenuItem Item1Sub2 = MenuItem("Interval");
MenuItem Item1Sub2Sub1 = MenuItem("Interval =");
MenuItem Item1Sub3 = MenuItem("Switch duration");
MenuItem Item1Sub3Sub1 = MenuItem("Switch dur =");

MenuItem Item2 = MenuItem("Program 2");
MenuItem Item2Sub1 = MenuItem("Angle");
MenuItem Item2Sub1Sub1 = MenuItem("Angle =");
MenuItem Item2Sub2 = MenuItem("Speed");
MenuItem Item2Sub2Sub1 = MenuItem("Speed =");

MenuItem Item3 = MenuItem("Program 3");
MenuItem Item3Sub1 = MenuItem("Angle");
MenuItem Item3Sub1Sub1 = MenuItem("Angle =");
MenuItem Item3Sub2 = MenuItem("Interval");
MenuItem Item3Sub2Sub1 = MenuItem("Interval =");

MenuItem Item4 = MenuItem("Program 4");
MenuItem Item4Sub1 = MenuItem("Angle");
MenuItem Item4Sub1Sub1 = MenuItem("Angle =");
MenuItem Item4Sub2 = MenuItem("Interval");
MenuItem Item4Sub2Sub1 = MenuItem("Interval =");

MenuItem Item5= MenuItem("Firmware update");
MenuItem Item5Sub1 = MenuItem("Update mode");

MenuItem Item6= MenuItem("Info");
MenuItem Item6Sub1 = MenuItem("Version 1.3");

MenuItem Item7= MenuItem("Credits");
MenuItem Item7Sub1 = MenuItem("Mimetas");

//this function builds the menu and connects the correct items together
void menuSetup(){
    Serial.println("Setting up menu...");   //add the file menu to the menu root
    menu.getRoot().add(Item1); //setup the settings menu item   //Item 1...
    Item1.addRight(Item2).addRight(Item3).addRight(Item4).addRight(Item5).addRight(Item6).addRight(Item7);
    //Item 1...
    Item1.add(Item1Sub1).addRight(Item1Sub2).addRight(Item1Sub3);
    Item1Sub1.add(Item1Sub1Sub1); Item1Sub2.add(Item1Sub2Sub1); Item1Sub3.add(Item1Sub3Sub1);
    //Item 2...
    Item2.add(Item2Sub1).addRight(Item2Sub2);
    Item2Sub1.add(Item2Sub1Sub1); Item2Sub2.add(Item2Sub2Sub1);
    //Item 3...
    Item3.add(Item3Sub1).addRight(Item3Sub2);
    Item3Sub1.add(Item3Sub1Sub1); Item3Sub2.add(Item3Sub2Sub1);
    //Item 4...
    Item4.add(Item4Sub1).addRight(Item4Sub2);
    Item4Sub1.add(Item4Sub1Sub1); Item4Sub2.add(Item4Sub2Sub1);
    //Item 5...
    Item5.add(Item5Sub1);
    //Item 6...
    Item6.add(Item6Sub1);
    //Item 7...
    Item7.add(Item7Sub1);
}
/*****************************************************************
                  Use item menu (MenuBackend)
*****************************************************************/
void menuUseEvent(MenuUseEvent used){

    bool test = true;
    lcd.setCursor(0,1);

    if (used.item == Item1){ //comparison agains a known item
        lcd.print("-> P1 selected");
        delay(1250);
        progStatus = 1;
        EEPROM.write(memProgAddr, progStatus);
        menu.toRoot();
    }

    if (used.item == Item2){ //comparison agains a known item
        lcd.print("-> P2 selected");
        delay(1250);
        progStatus = 2;
        EEPROM.write(memProgAddr, progStatus);
        menu.toRoot();
    }

    if (used.item == Item3){ //comparison agains a known item
        lcd.print("-> P3 selected");
        delay(1250);
        progStatus = 3;
        EEPROM.write(memProgAddr, progStatus);
        menu.toRoot();
    }

    if (used.item == Item4){ //comparison agains a known item
        lcd.print("-> P4 selected");
        delay(1250);
        progStatus = 4;
        EEPROM.write(memProgAddr, progStatus);
        menu.toRoot();
    }

    if (used.item == Item1Sub1Sub1){ //comparison agains a known item
        lcd.print("-> Angle = "); lcd.print(angle); lcd.print((char)223);
        delay(1250);
        menu.toRoot();
    }

    if (used.item == Item1Sub2Sub1){ //comparison agains a known item
        lcd.print("-> Interval = "); lcd.print(interval); lcd.print(" "); lcd.print("m");
        delay(1250);
        menu.toRoot();
    }

    if (used.item == Item1Sub3Sub1){ //comparison agains a known item
        lcd.print("-> Switch dur = "); lcd.print(switch_duration); lcd.print(" "); lcd.print("s");
        delay(1250);
        menu.toRoot();
    }

    //if (used.item == Item2Sub1Sub1){ //comparison agains a known item
    //    lcd.print("-> Angle = "); lcd.print(angle); lcd.print(" "); lcd.print((char)223);
    //    delay(1250);
    //    menu.moveUp(); menu.moveUp();
    //}

    if (used.item == Item2Sub2Sub1){ //comparison agains a known item
        lcd.print("-> Speed = "); lcd.print(speed); lcd.print(" "); lcd.print("s");
        delay(1250);
        menu.toRoot();
    }

    //if (used.item == Item3Sub1Sub1){ //comparison agains a known item
    //    lcd.print("-> Angle = "); lcd.print(angle); lcd.print(" "); lcd.print((char)223);
    //    delay(1250);
    //    menu.moveUp(); menu.moveUp();
    //}

    //if (used.item == Item3Sub2Sub1){ //comparison agains a known item
    //    lcd.print("-> Interval = "); lcd.print(interval); lcd.print(" "); lcd.print("s");
    //    delay(1250);
    //    menu.moveUp(); menu.moveUp();
    //}

    //if (used.item == Item4Sub1Sub1){ //comparison agains a known item
    //    lcd.print("-> Angle = "); lcd.print(angle); lcd.print(" "); lcd.print((char)223);
    //    delay(1250);
    //    menu.moveUp(); menu.moveUp();
    //}

    //if (used.item == Item4Sub2Sub1){ //comparison agains a known item
    //    lcd.print("-> Interval = "); lcd.print(interval); lcd.print(" "); lcd.print("s");
    //    delay(1250);
    //    menu.moveUp(); menu.moveUp();
    //}

    if (used.item == Item5Sub1){ //comparison agains a known item
        Particle.connect();
        lcd.print("-> Update mode");
        delay(1250);
        menu.toRoot();
    }

}

/*****************************************************************
                 Menu navigation (MenuBackend)
*****************************************************************/
void menuChangeEvent(MenuChangeEvent changed){
    MenuItem newMenuItem=changed.to; //get the destination menu
      // (changed.from.getName());
      // (changed.to.getName())
    lcd.clear();
    lcd.setCursor(0,1);
    lcd.print(changed.to.getName());
    lcd.setCursor(0,0);

    if(newMenuItem.getName()==menu.getRoot()){
        lcd.print("    pRocker v1.3");
        lcd.setCursor(0,1);
        lcd.print("Main Menu");
    }

    else if(newMenuItem.getName()=="Program 1" || newMenuItem.getName()=="Program 2" || newMenuItem.getName()=="Program 3" || newMenuItem.getName()=="Program 4" || newMenuItem.getName()=="Firmware update" || newMenuItem.getName()=="Info" || newMenuItem.getName()=="Credits"){
        lcd.print("      Main Menu");
    }
    else if(newMenuItem.getName()=="Angle" || newMenuItem.getName()=="Interval" || newMenuItem.getName()=="Switch duration"){
        lcd.print("     Program 1-4");
    }

    else if(newMenuItem.getName()=="Angle" || newMenuItem.getName()=="Speed"){
        lcd.print("     Program 1-4");
    }

    else if(newMenuItem.getName()=="Angle" || newMenuItem.getName()=="Interval"){
        lcd.print("     Program 1-4");
    }

    else if(newMenuItem.getName()=="Angle" || newMenuItem.getName()=="Interval"){
        lcd.print("     Program 1-4");
    }

    else if(newMenuItem.getName()=="Angle ="){
        lcd.print("     Program 1-4");
        varselect = 1;
        angle = change (angle);
        EEPROM.write(memAngleAddr, angle);
        Serial.println("Changing Angle");
    }

    else if(newMenuItem.getName()=="Interval ="){
        lcd.print("     Program 1-4");
        varselect = 2;
        interval = change (interval);
        EEPROM.write(memIntervalAddr, interval);
        Serial.println("Changing Interval");
    }

    else if(newMenuItem.getName()=="Switch dur ="){
        lcd.print("      Program 1");
        varselect = 3;
        switch_duration = change (switch_duration);
        EEPROM.write(memSwitchAddr, switch_duration);
        Serial.println("Changing Test duration");
    }

    else if(newMenuItem.getName()=="Speed ="){
        lcd.print("       Program 2");
        varselect = 4;
        speed = change (speed);
        EEPROM.write(memSpeedAddr, speed);
        Serial.println("Changing Speed");
    }

    else if(newMenuItem.getName()=="Update mode"){
                    lcd.print(" Firmware update");
    }

    else if(newMenuItem.getName()=="Version 1.3"){
                    lcd.print("V1.3 20180111 BW");
    }
    else if(newMenuItem.getName()=="Mimetas"){
                    lcd.print("         Credits");
    }
}

 /*****************************************************************
                         Read LCD buttons
 *****************************************************************/
void ReadButtons(){  //read buttons status
    //Serial.println("VOID ReadButtons");
    lastButtonPushed = 0;
    uint8_t buttons = lcd.readButtons();

    if (buttons) {
        if (buttons & BUTTON_UP){
            lastButtonPushed = 2;
            timer_standby = 0;
            Serial.println("UP pushed");
            delay(buttonPush);
        }
        if (buttons & BUTTON_DOWN){
            lastButtonPushed = 8;
            timer_standby = 0;
            Serial.println("DOWN pushed");
            delay(buttonPush);
        }
        if (buttons & BUTTON_LEFT){
            lastButtonPushed = 4;
            timer_standby = 0;
            Serial.println("LEFT pushed");
            delay(buttonPush);
        }
        if (buttons & BUTTON_RIGHT){
            lastButtonPushed = 6;
            timer_standby = 0;
            Serial.println("RIGHT pushed");
            delay(buttonPush);
        }
        if (buttons & BUTTON_SELECT){
            lastButtonPushed = 9;
            timer_standby = 0;
            Serial.println("SELECT pushed");
            delay(buttonPush);
        }
    }
}

/*****************************************************************
              Navigate through menu (MenuBackend)
*****************************************************************/
void NavigateMenu(){
    //Serial.println("VOID NavigateMenu");
    //Serial.println(lastButtonPushed);
    MenuItem currentMenu=menu.getCurrent();

    if (lastButtonPushed != 0){// so if nothing happend then don't waste it's time
        switch (lastButtonPushed) {
            case 8: menu.moveRight(); Serial.println("Case DOWN"); break;
            case 2: menu.moveLeft(); Serial.println("Case UP"); break;
            case 9: menu.use(); Serial.println("Case SELECT"); break;

            case 4: //menu.moveBack(); break;
                if ((currentMenu.moveUp()))
                    {menu.moveUp(); Serial.println("Case LEFT"); break;}
                else
                    {menu.moveBack(); Serial.println("Case LEFT"); break;}

            case 6:
                if ((currentMenu.moveDown()))
                menu.moveDown(); Serial.println("Case RIGHT"); break;
        }
    }
}

/*****************************************************************
              Change parameter values (MenuBackend)
*****************************************************************/
double change (double variable){
    Serial.println("VOID Change");

    if (varselect == 1){
        variable = angle;
        Str = "Angle = " ;
    }
    else if (varselect == 2){
        variable = interval;
        Str = "Interval = ";
    }
    else if (varselect == 3){
        variable = switch_duration;
        Str = "Switch dur = ";
    }

    else if (varselect == 4){
        variable = speed;
        Str = "Speed = ";
    }

    while (true){
        lcd.setCursor(0,1);
        lcd.print(Str); lcd.print(variable,1); lcd.print(" ");

        uint8_t buttons = lcd.readButtons();

        if (buttons) {
            if (buttons & BUTTON_UP){
                if (varselect == 1){
                    variable = constrain(variable,-21,21);
                    variable = variable + 1;
                    Serial.println("Variable 1 UP");
                }
                else if (varselect == 2){
                    variable = constrain(variable,2,60);
                    variable = variable + 1;
                    Serial.println("Variable 2 UP");
                }
                else if (varselect == 3){
                    variable = constrain(variable,0,60);
                    variable = variable + 1;
                    Serial.println("Variable 3 UP");
                }
                else if (varselect == 4){
                    variable = constrain(variable,10,600);
                    variable = variable + 10;
                    Serial.println("Variable 4 UP");
                }
                delay(buttonPush);
            }
            if (buttons & BUTTON_DOWN){
                if (varselect==1){
                    variable = constrain(variable,-21,21);
                    variable = variable - 1;
                    Serial.println("Variable 1 DOWN");
                }
                else if (varselect==2){
                    variable = constrain(variable,2,60);
                    variable = variable - 1;
                    Serial.println("Variable 2 DOWN");
                }
                else if (varselect==3){
                    variable = constrain(variable,0,60);
                    variable = variable - 1;
                    Serial.println("Variable 3 DOWN");
                }
                else if (varselect == 4){
                    variable = constrain(variable,10,600);
                    variable = variable - 10;
                    Serial.println("Variable 4 DOWN");
                }
                delay(buttonPush);
            }

            if (buttons & BUTTON_SELECT){
                return variable;
            }
        }
    }
}

/*****************************************************************
                          Void setup
*****************************************************************/

void setup(){
    // Setting up Serial com, lcd, wire com, ADXL345 com & settings
    Serial.begin(9600); delay(1);
    lcd.begin(16, 2); delay(1);
    Wire.begin(); delay(1);
    sensorInitiate(); delay(1);       // Initiate ADXL345 sensor
    Accel.set_bw(ADXL345_BW_12);

    // Defining pinMode motor pins, well volume sensors
    pinMode(PIN_M_DIR, OUTPUT);     // Controls direction
    pinMode(PIN_M_SPEED, OUTPUT);     // Controls power to motor
    pinMode(PIN_HOME,INPUT);
    pinMode(PIN_F_W1,INPUT);
    pinMode(PIN_F_W2,INPUT);
    digitalWrite(PIN_M_DIR,LOW);
    digitalWrite(PIN_M_SPEED,LOW);

    // Defining rocker parameters incl. angle, interval, etc.
    counter = 0; counter2 = 0;
    angle = 7; interval = 8; switch_duration = 10; speed = 10;
    step = 360/3200; conversion = 0.0342; //a=0.0309 and b=-22.988 for -14 to 14 linear regression || a=0.0341 b=-25.13 f0r -7 to 7 linear regression
    stepTotal = (2*angle)/conversion;
    stepCalc = (-angle+25.13)/0.0342;
    Time.zone(1);

    // Run mimetasIntro, homePosition to 0 degrees and setup menu structure.

    progStatus = EEPROM.read(memProgAddr);
    if (progStatus == 0){
        mimetasIntro();         // Intro screen Mimetas
        homePosition(1000);
        menuSetup();            // menuSetup (backendMenu)
        menu.toRoot();          //
    }

    if (progStatus != 0){
        angle = EEPROM.read(memAngleAddr);
        interval = EEPROM.read(memIntervalAddr);
        switch_duration = EEPROM.read(memSwitchAddr);
        speed = EEPROM.read(memSpeedAddr);
    }
}

/*****************************************************************
                          Void loop
*****************************************************************/
void loop(){
    progStatus = EEPROM.read(memProgAddr);
    if (progStatus == 0){
        if (programReset == 1){
            menu.toRoot();
            programReset = 0;
        }
        ReadButtons();
        NavigateMenu();
        timer_standby = timer_standby+1;

        // Rocker standby function

        /*********
        if (timer_standby > 10000){
            lcd.clear();
            lcd.setCursor(0,0);
            lcd.print("pRocker v1.3    ");
            while (timer_standby > 1){
                time_refresh = time_refresh + 1;
                buttons=lcd.readButtons();
                if (time_refresh>2000){
                    lcd.setCursor(0,1);
                    lcd.print(Time.format(Time.now(), "%Y-%m-%d %H:%M:%S"));
                    time_refresh = 0;
                }
                if (buttons != 0){
                    timer_standby = 0;
                    break;
                }
            }
            menu.toRoot();
        }
        *******/

    }

    // Return from program to menu feature
    if (progStatus != 0){
        timer = 0;
        buttons = lcd.readButtons();
        while (buttons & BUTTON_LEFT){
            buttons = lcd.readButtons();
            timer = timer + 1;
            if (timer > 1000){                //
                progStatus = 0;
                programReset = 1;
                progShowProgram = 0;
                EEPROM.write(memProgAddr, progStatus);
                lcd.clear();
                digitalWrite(PIN_M_DIR,LOW);
                menu.toRoot();
                homePosition(1000);
                timer = 0;
                break; break;
            }
        }
    }

    runProgram();
}


/*****************************************************************
                     Void runProgram
*****************************************************************/
void runProgram(){

/****************************************
    PROGRAM 1 - Standard Mimetas P1
****************************************/
    if (progStatus == 1){
        if(progShowProgram == 0){
            lcd.clear(); lcd.setCursor(0,0);
            lcd.print("Program 1 active");
            lcd.setCursor(0,1);
            lcd.print("Ang "); lcd.print(angle); lcd.print((char)223); lcd.print(" Int "); lcd.print(interval); lcd.print("m");
            progShowProgram = 1;
        }

        runMotor(100);
        angleMeasurement();
        if (pitch > angle && direction == LOW){
            direction = HIGH;
            delay(interval*60*1000);
        }
        if (pitch < -angle && direction == HIGH){
            direction = LOW;
            delay(interval*60*1000);
        }

    }
/******************************************
      PROGRAM 2 - Continuous rocking
*******************************************/
    if (progStatus == 2){

        if(progShowProgram == 0){
            lcd.clear();lcd.setCursor(0,0);
            lcd.print("Program 2 active");
            lcd.setCursor(0,1);
            lcd.print("Ang "); lcd.print(angle); lcd.print((char)223); lcd.print(" "); lcd.print("Sp "); lcd.print(speed); lcd.print(" "); lcd.print("S/r");
            progShowProgram = 1;
        }

        runMotor(100);
        delay((speed*1000)/3200);
        //angleMeasurement();
        //if (pitch > angle){
        //    direction = HIGH;
        //}
        //if (pitch < -angle){
        //    direction = LOW;
        //}
    }

/***************************************
    PROGRAM 3 - Unidirectional shear
***************************************/
    if (progStatus == 3){

        if(progShowProgram == 0){
            lcd.clear();lcd.setCursor(0,0);
            lcd.print("Program 3 active");
            lcd.setCursor(0,1);
            lcd.print("Ang "); lcd.print(angle); lcd.print((char)223); lcd.print(" Int "); lcd.print(interval); lcd.print("m");
            stepTotal = (2*angle)/conversion;
            progShowProgram = 1;
            setAngleStatus = 0;
        }

        if (setAngleStatus == 0){
            setAngle(angle,2000);
            setAngleStatus = 1;
        }

        angleMeasurement();
        if (pitch>angle){
           direction = 1;
        }
        if (pitch<-angle){
           direction = 0;
        }

        runMotor(10);
        if (direction == 1){
            delay((interval*60*1000)/stepTotal);  // Calculates delay using the interval and stepTotal.
        }
    }

/******************************************
    PROGRAM 4 - Continuous flow rate
*******************************************/
    if (progStatus == 4){
        if (angle > 14){
            lcd.clear();lcd.setCursor(0,0);
            lcd.print("Program 4 warning");
            lcd.setCursor(0,1);
            lcd.print("Ang "); lcd.print(angle); lcd.print((char)223); lcd.print(" > "); lcd.print("14"); lcd.print((char)223);
            delay(2000);
            progStatus = 0;
            progShowProgram = 0;
            menu.toRoot();
        }

        if(progShowProgram == 0){
            lcd.clear();lcd.setCursor(0,0);
            lcd.print("Program 4 active");
            lcd.setCursor(0,1);
            lcd.print("Ang "); lcd.print(angle); lcd.print((char)223); lcd.print(" Int "); lcd.print(interval); lcd.print("m");
            stepTotal = (2*angle)/conversion;
            progShowProgram = 1;
        }
        //counter = counter + 1;
        angleMeasurement();
        if (pitch>angle){
           direction = 1;
        }
        if (pitch<-angle){
           direction = 0;
        }

        runMotor(100);
        delay((interval*60*1000)/stepTotal);
        //String pubstring = String(counter) + " " + String(pitch);
        //Particle.publish("pubdata",String(pubstring));
    }
}

/*****************************************************************
                        Void mimetasIntro
*****************************************************************/
void mimetasIntro (){
    lcd.createChar(1,car1); lcd.createChar(2,car2); lcd.createChar(3,car3);
    lcd.createChar(4,car4); lcd.createChar(5,car5); lcd.createChar(6,car6);
    lcd.setCursor(16,0); lcd.write(1); lcd.write(2); lcd.write(3);
    lcd.setCursor(16,1);lcd.write(4);  lcd.write(5);  lcd.write(6);
    for (int positionCounter = 0; positionCounter < 16; positionCounter++) {
        lcd.scrollDisplayLeft(); delay(150);
    }
    lcd.setCursor(25,0); lcd.print("Mimetas"); lcd.setCursor(20,1); lcd.print("pRocker v1.3");
    delay(2000);
}

/*****************************************************************
                           Void runMotor
*****************************************************************/
void runMotor(int delay){
    digitalWrite(PIN_M_DIR,direction);
    digitalWrite(PIN_M_SPEED,HIGH);
    delayMicroseconds(delay);
    digitalWrite(PIN_M_SPEED,LOW);
    delayMicroseconds(delay);
}

/*****************************************************************
                        Void runMotorDelay
*****************************************************************/
void runMotorDelay(int m_delay,int t_delay){
    digitalWrite(PIN_M_DIR,direction);
    digitalWrite(PIN_M_SPEED,HIGH);
    delayMicroseconds(m_delay);
    digitalWrite(PIN_M_SPEED,LOW);
    delayMicroseconds(m_delay);
    delay(t_delay);
}

/*****************************************************************
                        Void runMotorCount
*****************************************************************/
void runMotorCount(int delay, int setCount){
    counter2 = 0;
    while (counter2 < setCount){
        counter2++;
        digitalWrite(PIN_M_DIR,direction);
        digitalWrite(PIN_M_SPEED,HIGH);
        delayMicroseconds(delay);
        digitalWrite(PIN_M_SPEED,LOW);
        delayMicroseconds(delay);
    }
}

/*****************************************************************
                    Void angleMeasurement
*****************************************************************/
void angleMeasurement(){
    Accel.readAccel(&X,&Y,&Z);
    pitch = (atan2((X-mofX),(sqrt((Y-mofY)*(Y-mofY)+(Z-mofZ)*(Z-mofZ)))) * 180.0) / M_PI;
}

/*****************************************************************
                    Void setAngle
*****************************************************************/
void setAngle(double val_angle, int val_timing){
    while (pitch > (val_angle+angle_margin) || pitch < (val_angle-angle_margin)){
        runMotor(val_timing);
        angleMeasurement();
    }
}

/*****************************************************************
                    Void setAngleStep
*****************************************************************/
void setAngleStep(int val_timing){
    homePosition(val_timing);
    runMotorCount(val_timing,int(stepCalc)); // initiate
}
/*****************************************************************
                      Void homePositionAng
*****************************************************************/
void homePositionAng(int val_timing){
    while (homePos == 0){
        runMotor(val_timing);
        angleMeasurement();
        if (pitch < angle_margin && pitch > -angle_margin){
            homePos = 1;
        }
    }
}

/*****************************************************************
                      Void homePosition
*****************************************************************/
void homePosition(int val_timing){
    homePos = digitalRead(PIN_HOME);
    while (homePos == 1){
        runMotor(val_timing);
        homePos = digitalRead(PIN_HOME);
    }
    runMotorCount(2000,732);
}

/*****************************************************************
                      Void volumeInit
*****************************************************************/
void volumeInit(int samples){
    int initmeas1 [samples];
    int initmeas2 [samples];
    for (int i = 0; i < samples; i++){
        initmeas1[i] = analogRead(PIN_F_W1);
        initmeas2[i] = analogRead(PIN_F_W2);
    }
}

/*****************************************************************
                      Void volumeMeas
*****************************************************************/
void volumeMeas(){
    meas1 = analogRead(PIN_F_W1);
    meas2 = analogRead(PIN_F_W2);
}

/*****************************************************************
                Void sensorInitiate (ADXL345)
*****************************************************************/
void sensorInitiate(){
    Wire.beginTransmission(ADXLaddr);   // Start I2C transmission
    Wire.write(0x2C);                   // Select bandwidth rate register
    Wire.write(0x0A);                   // Select output data rate = 100 Hz
    Wire.endTransmission();             // Stop I2C Transmission

    Wire.beginTransmission(ADXLaddr);   // Start I2C transmission
    Wire.write(0x2D);                   // Select power control register
    Wire.write(0x08);                   // Select auto sleep disable
    Wire.endTransmission();             // Stop I2C transmission

    Wire.beginTransmission(ADXLaddr);   // Start I2C transmission
    Wire.write(0x31);                   // Select data format register
    Wire.write(0x08);                   // Select full resolution, +/-2g
    Wire.endTransmission();             // End I2C transmission
    delay(300);
}
